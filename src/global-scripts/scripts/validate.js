const form = document.querySelector('#form');
const nameInput = document.querySelector('#nameInput');
const numberInput = document.querySelector('#numberInput');
const dateInput = document.querySelector('#dateInput');
const cvvInput = document.querySelector('#cvvInput');


nameInput.addEventListener('input', validateName);
nameInput.addEventListener('blur', validateName);

numberInput.addEventListener('input', validateNumber);
numberInput.addEventListener('blur', validateNumber);

dateInput.addEventListener('input', validateDate);
dateInput.addEventListener('blur', validateDate);

cvvInput.addEventListener('input', validateCvv);
cvvInput.addEventListener('blur', validateCvv);

form.addEventListener('submit', submitForm);

function validateName(event) {
    const nameValue = event.target.value.trim();
    const isValid = /^[a-zA-Z\s]+$/.test(nameValue);

    if (isValid) {
        event.target.setCustomValidity('');
    } else {
        event.target.setCustomValidity('Incorrect format. Enter only letters.')
    }
}

function validateNumber(event) {
    const numberValue = event.target.value.trim();
    const isValid = /\b\d{4}[ -]?\d{4}[ -]?\d{4}[ -]?\d{4}\b/.test(numberValue);

    if (isValid) {
        event.target.setCustomValidity('');
    } else {
        event.target.setCustomValidity('Incorrect format. Number must have 16 digits.');
    }
}

function validateDate(event) {
    const dateValue = event.target.value.trim();
    const isValid = /^(0[1-9]|1[0-2])\/?([0-9]{2})$/.test(dateValue);

    if (isValid) {
        event.target.setCustomValidity('');
    } else {
        event.target.setCustomValidity('Invalid date format. Enter date as MM/YY.');
    }
}

function validateCvv(event) {
    const cvvValue = event.target.value.trim();
    const isValid = /^\d{3}$/.test(cvvValue);

    if (isValid) {
        event.target.setCustomValidity('');
    } else {
        event.target.setCustomValidity('Invalid CVV format. Enter 3 digits.')
    }
}


function submitForm(event) {
    event.preventDefault();
    const form = event.target;
    const isValid = form.checkValidity();
    const nextStepButton = form.querySelector('button[type="submit"]');

    nextStepButton.classList.add('disabled');
    nextStepButton.setAttribute('disabled', 'disabled');

    if (isValid) {
        alert('Form submitted successfully!');

        nextStepButton.classList.remove('disabled');
        nextStepButton.removeAttribute('disabled');
    } else {
        event.target.setCustomValidity = 'Please fill out all required fields correctly.';
        form.reportValidity();
    }
}
