/// Main JavaScript File
/// Here we import all the global JavaScript files we need for our project.

import '../global-styles/style.scss'

const inputs = document.querySelectorAll('input[type="text"], input[type="number"]');

inputs.forEach(input => {
  const label = input.previousElementSibling;
  input.addEventListener('input', () => {
    if (input.value.trim() !== '') {
      label.classList.add('visible');
    } else {
      label.classList.remove('visible');
    }
  });
});